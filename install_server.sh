#! /bin/bash
echo "Installing server"

virtualenv venv
source venv/bin/activate
pip install pyserial
ln -s /usr/lib/python2.7/dist-packages/numpy* $VIRTUAL_ENV/lib/python*/site-packages
ln -s /usr/lib/python2.7/dist-packages/smbus* $VIRTUAL_ENV/lib/python*/site-packages
ln -s /usr/lib/python2.7/dist-packages/RPi* $VIRTUAL_ENV/lib/python*/site-packages
npm install
echo "now run with 'node index.js' or 'DEBUG=pythonic node index.js" 
