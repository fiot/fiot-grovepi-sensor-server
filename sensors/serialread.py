import time,glob
import serial

baud = 9600
port_base="/dev/ttyACM*"  # for Feather M0s
#port_base="/dev/ttyUSB*" # for Arduino UNO, etc

class SerialPort():
    def getReading(self):
        try:
            port=glob.glob(port_base)[0]
            ser = serial.Serial(port,baud)
            ser.flushInput()
            ser.flushOutput()
            time.sleep(.1)
            result=ser.readline().decode('utf-8')
            result=result.rstrip() #remove trailing chars
            ser.close()
            return result
        except Exception as exc:
            print("# WARNING caught exception: %s" % exc)
            
