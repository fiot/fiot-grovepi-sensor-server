import time
import grovepi
import atexit

atexit.register(grovepi.dust_sensor_dis)

grovepi.dust_sensor_en()


class OneWireSensor():
    def getReading(self): 
	while True:
		try:
			[new_val,lowpulseoccupancy] = grovepi.dustSensorRead()
			if new_val:
				return float(lowpulseoccupancy)/10.0
			time.sleep(1) 
			
		except IOError:
			print ("Error")



