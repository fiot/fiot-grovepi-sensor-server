# fiot-grovepi-sensor-server

A nodejs - based server on top of grovepi.  For python sensor drivers placed in '/sensors' and which follow the proper format (see examples), this server provides a straightforward HTTP interface for returning their values.

# get the code

`git clone https://gitlab.com/fiot/fiot-grovepi-sensor-server.git`

# install server

`./install_server.sh`

# update firmware

`./update_firmware.sh`

# run server

`./run_server.sh`

# use

- list all available sensors with:

`[yourLocalIPAddress]:3000/list`

- return all sensor values with:

`[yourLocalIPAddress]:3000/list`

- return a particular sensor value with (e.g. for a sensor driver named 'temp'):

`[yourLocalIPAddress]:3000/sensors/temp'


# Acknowledgements:

- original sensor server written by Parker Woodworth
- code source: https://gitlab.com/fiot/fiot-sensor-server

